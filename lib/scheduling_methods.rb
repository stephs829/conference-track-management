module Conference

  module SchedulingMethods

    def self.convert_time_str_to_obj(string)
      t = Time.now
      Time.new(t.year, t.month, t.day, string[0,2].to_i, string[2,4].to_i, 0, 0)
    end

    def self.calculate_init_tracks(talks, session_types)
      track = Conference::Track.new(session_types)
      n = (talks.total_by_time.to_f / track.get_length.to_f).ceil - 1
      n.times do Conference::Track.new(session_types) end
    end

    def self.sort_talks_into_sessions(talks, sessions)
      talks.each do |talk|
        i = 0
        sessions = sessions.dup.sort_by{ |a| a.talks_running_time }
        sessions.each do |session|
          #continue if session is long enough for talk to be added, else check next session
          (session.talks_running_time + talk.time <= session.session_length) ? break : i += 1
        end
        sessions[i] ? (sessions[i].add_talk talk) : (raise ArgumentError, "Cannot fit talk into available sessions")
      end
    end

  end

end