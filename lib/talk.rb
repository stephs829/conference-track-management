module Conference
  
  class Talk
    ###belongs to session###
    #input_title - string
    #time - integer

    attr_accessor :session
    attr_reader :input_title, :time

    INTEGER_REGEX = /\d+/
    LIGHTNING_REGEX = /\blightning\b/

    def initialize(input_title, time=nil)
      @input_title = input_title
      @time = time ? time : parse_input_title(input_title)
    end

    def parse_input_title(input_title)
      if input_title[INTEGER_REGEX]
        time = input_title[INTEGER_REGEX].to_i 
      elsif input_title[LIGHTNING_REGEX]
        time = 5
      else 
        raise ArgumentError, "Cannot parse time from input string"
      end
    end

  end

end