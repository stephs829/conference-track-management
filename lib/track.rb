module Conference

	class Track
    ###has many sessions###
    #id - integer

    attr_reader :sessions, :array, :counter

    @@array = []
    @@count = 0

    def initialize(session_types)
      add_all_session_types(session_types)
      @counter = @@count += 1
      @@array << self
    end

    def add_all_session_types(session_types)
      session_types.each { |session|
        session = Conference::Session.new(session[:start_time], session[:min_end_time], session[:max_end_time], session[:end_event])
        self.add_session session
      }
    end

    def add_session(session)
      @sessions ||= []
      @sessions << session
      session.track = self
    end

    def get_length
      self.sessions.inject(0) { |total, session| 
        total + session.session_length
      }
    end

    def self.array
      @@array
    end

    def format_title
      puts "Track " + self.counter.to_s + ":"
    end

    def format_line(time, title)
      time = time.strftime("%I:%M%p")
      time.to_s + ' ' + title
    end

    def format_sessions
      self.sessions.each do |session|
        start_time = Conference::SchedulingMethods.convert_time_str_to_obj(session.start_time)
        max_end_time = Conference::SchedulingMethods.convert_time_str_to_obj(session.max_end_time)
        min_end_time = Conference::SchedulingMethods.convert_time_str_to_obj(session.min_end_time) if session.min_end_time

        session.talks.each do |talk|
          puts format_line(start_time, talk.input_title)
          start_time += (talk.time * 60)
        end

        if min_end_time
          #if last talk ends after min end time (4PM) --> "5:00PM Networking Event", else "4:00PM Networking Event"
          end_time = (start_time > min_end_time) ? max_end_time : min_end_time
        else 
          end_time = max_end_time
        end
        puts format_line(end_time, session.end_event)
      end
    end

	end

end