module Conference
  
  class Talks
    attr_reader :talks

    def initialize talks
      @talks = talks
    end

    def sort_descending_by_time
      @talks = @talks.dup.sort_by{ |a| a.time }.reverse!
    end

    def total_by_time
      @talks.inject(0) { |total, x| total + x.time }
    end

    def [](ind)
      @talks[ind]
    end

    def <<(val)
      @talks << val
    end

    def each(&block)
      @talks.each(&block)
    end

  end
end