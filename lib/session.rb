module Conference

	class Session
    ###belongs to track###
    ###has many talks###
    #start time - string, 24h format
    #min end time - string, 24h format #optional
    #max end time - string, 24h format 
    #end event - string 
    #session_length - integer
    #talks_running_time - integer

    attr_accessor :track
    attr_reader :talks, :array, :start_time, :min_end_time, :max_end_time, :end_event, :session_length, :talks_running_time

    @@array = []

    SESSION_TYPES = [ 
      { start_time: "0900", min_end_time: nil, max_end_time: "1200", end_event: "Lunch" },
      { start_time: "1300", min_end_time: "1600", max_end_time: "1700", end_event: "Networking Event" }
      #can add more sessions here, in order of appearance
    ]

    def initialize(start_time, min_end_time, max_end_time, end_event)
      @start_time = start_time
      @min_end_time = min_end_time if min_end_time
      @max_end_time = max_end_time
      @end_event = end_event
      @session_length = get_session_length(start_time, max_end_time)
      @talks_running_time = 0
      @@array << self
    end

    def add_talk(talk)
      @talks ||= []
      @talks_running_time += talk.time
      @talks << talk
      talk.session = self
    end

    def self.array
      @@array
    end
    
    def get_session_length(start_time_str, max_end_time_str)
      session_length = Conference::SchedulingMethods.convert_time_str_to_obj(max_end_time_str) - Conference::SchedulingMethods.convert_time_str_to_obj(start_time_str)
      (session_length / 60).to_i
    end

	end

end

