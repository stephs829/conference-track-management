require_relative 'talk'
require_relative 'session'
require_relative 'track'
require_relative 'talks'
require_relative 'scheduling_methods'

#TODO: Move parse input into a model so can specify a variety of inputs
input = File.readlines(ARGV[0])
talks = Conference::Talks.new(input.map{|text| Conference::Talk.new(text.chomp("\n"))} )
talks.sort_descending_by_time
#Calculate how many tracks with all session types are needed to contain talks and init
Conference::SchedulingMethods.calculate_init_tracks(talks, Conference::Session::SESSION_TYPES)
#Sort by adding longest talk to session with lowest talk time so far that fits
Conference::SchedulingMethods.sort_talks_into_sessions(talks, Conference::Session.array)
#Format and output tracks
Conference::Track.array.each do |track|
  track.format_title
  track.format_sessions
  puts "" 
end