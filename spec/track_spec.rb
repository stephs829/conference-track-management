require 'spec_helper'
require 'scheduling_methods'
require 'track'
require 'session'
require 'talk'

describe Conference::Track do

  VALID_SESSION_TYPES = [ { start_time: "0900", min_end_time: nil, max_end_time: "1200", end_event: "Lunch" },
                          { start_time: "1300", min_end_time: "1600", max_end_time: "1700", end_event: "Networking Event" } ]

  INVALID_SESSION_TYPES = [ { start_time: nil, min_end_time: nil, max_end_time: "1200", end_event: "Lunch" } ]

  describe "#initialize" do
    context "with valid session types" do
      let(:track) { Conference::Track.new(VALID_SESSION_TYPES) }
      it "instantiates Track with two sessions" do
        expect(track.sessions.length).to eq 2
      end
    end
    context "with invalid session types" do
      let(:track) { Conference::Track.new(INVALID_SESSION_TYPES) }
      it "does not instantiate Track" do
        expect{track}.to raise_error(NoMethodError)
      end
    end
  end

  describe "#add_all_session_types" do 
    context "accessing method directly" do
      it "does not initiate session" do
        expect{ Conference::Track.add_all_session_types(VALID_SESSION_TYPES) }.to raise_error(NoMethodError)
      end
    end
  end

  describe "#get_track_length" do 
    let(:track) { Conference::Track.new(VALID_SESSION_TYPES) }
    it "calculates length of track in minutes based on session inputs" do
      expect(track.get_length).to eq 420
    end
  end
end