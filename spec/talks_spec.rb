require 'spec_helper'
require 'talks'
require 'talk'

describe Conference::Talks do
  let(:input) { File.readlines('data/test_input.txt') }
  let(:talks) { Conference::Talks.new(input.map{|text| Conference::Talk.new(text.chomp("\n"))} ) }

  describe "talks" do 
    it "sorts talks" do
      talks.sort_descending_by_time
      expect(talks[4].time).to eq 60
    end

    it "gets total length of talks" do
      total = talks.total_by_time
      expect(total).to eq 785
    end
  end

end