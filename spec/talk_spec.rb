require 'spec_helper'
require 'talk'

describe Conference::Talk do
  let(:talk) { Conference::Talk.new(input_title) }
  let(:input) { File.readlines('data/test_input.txt') }

  describe "#initialize" do
    context "with no input string" do
      let(:input_title) { nil }
      it "does not instantiate Talk" do
        expect{talk}.to raise_error(NoMethodError)
      end
    end
    context "with input string that has time" do
      let(:input_title) { "Writing Fast Tests Against Enterprise Rails 60min" }
      it "instantiates Talk" do
        expect(talk.input_title).to eq "Writing Fast Tests Against Enterprise Rails 60min" 
      end
      it "parses time from input string" do
        expect(talk.time).to eq 60
      end
    end
    context "with input string containing lightning" do
      let(:input_title) { "Rails for Python Developers lightning" }
      it "saves time as 5" do
        expect(talk.time).to eq 5
      end
    end
    context "with input string without time or lightning" do
      let(:input_title) { "Rails for Python Developers" }
      it "raises ArgumentError" do
        expect{talk}.to raise_error(ArgumentError, "Cannot parse time from input string")
      end
    end
    context "with input string and time integer" do
      let(:talk) { Conference::Talk.new("Writing Fast Tests Against Enterprise Rails", 60) }
      it "will save time integer directly" do
        expect(talk.time).to eq 60
      end
    end
  end

end