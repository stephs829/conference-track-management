require 'spec_helper'
require 'scheduling_methods'
require 'session'
require 'track'
require 'talk'
require 'talks'

describe Conference::SchedulingMethods do
  context "with test input" do
    SESSION_TYPES = [ { start_time: "0900", min_end_time: nil, max_end_time: "1200", end_event: "Lunch" },
                      { start_time: "1300", min_end_time: "1600", max_end_time: "1700", end_event: "Networking Event" } ]
    
    describe "#convert_time_str_to_obj" do 
      let(:track) { Conference::Track.new(SESSION_TYPES) }

      it "should convert time string to time object" do
        t = Time.now
        time_obj = Time.new(t.year, t.month, t.day, 9, 0, 0, 0)
        expect(Conference::SchedulingMethods.convert_time_str_to_obj(track.sessions[0].start_time)).to eq time_obj
      end
    end

    describe "#calculate_init_tracks" do 
      let(:input) { File.readlines('data/test_input.txt') }
      let(:talks) { Conference::Talks.new(input.map{|text| Conference::Talk.new(text.chomp("\n"))}) }

      it "should calculate the number of tracks with all session types to initalize to contain the inputted talks" do
        existing_tracks = Conference::Track.array.length
        Conference::SchedulingMethods.calculate_init_tracks(talks, SESSION_TYPES)
        tracks_init = Conference::Track.array.length - existing_tracks
        expect(tracks_init).to eq 2
      end
    end

    describe "#sort_talks_into_sessions" do 
      #TODO: add a few cases to test method is sorting correctly
    end
  end

end