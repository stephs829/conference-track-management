require 'spec_helper'
require 'session'
require 'talk'

describe Conference::Session do
  let(:session) { Conference::Session.new("0900", nil, "1200", "Lunch") } 

  describe "#initialize" do
    context "with validated inputs" do
      it "instantiates Session with start time" do
        expect(session.start_time).to eq "0900"
      end
      it "instantiates Session with max_end_time time" do
        expect(session.max_end_time).to eq "1200"
      end
      it "instantiates Session with end event" do
        expect(session.end_event).to eq "Lunch"
      end
      it "calculates session length" do
        expect(session.session_length).to eq 3*60
      end
      it "starts session talks running time at 0" do
        expect(session.talks_running_time).to eq 0
      end
    end
    context "without start time required field" do 
      let(:invalid_session) { Conference::Session.new(nil, nil, "1200", "Lunch") }
      it "does not instantiate Session" do 
        expect{invalid_session}.to raise_error(NoMethodError)
      end
    end 
  end

  describe "#add_talk" do 
    context "adding valid talk to Session" do
      let(:talk) { Conference::Talk.new("Writing Fast Tests Against Enterprise Rails 60min") } 
      it "adds talk to session's talk array" do
        session.add_talk(talk)
        session.talks.include? talk
        expect(talk.session).to eq session
        expect(session.talks_running_time).to eq 60
      end
    end
  end

  describe "#self.array" do 
    context "another session created" do
      it "increases @@array by 1" do 
        arr_count = Conference::Session.array.count
        session
        expect(Conference::Session.array.count).to eq arr_count + 1
      end
    end
  end

end