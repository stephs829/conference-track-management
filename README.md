##Conference Track Management##

###How to run this application###
- `git clone` this repo to your local dev machine
- Run `bundle install` to install rspec for testing
- To run the program with the given test input, type `ruby lib/conference_track_management.rb data/test_input.txt`
- To run the tests, type `rspec spec`

_Note: other test input files are included for testing purposes_

* `ruby lib/conference_track_management.rb data/test_input2.txt` has less talks so the last event will start at 4pm instead of 5pm
* `ruby lib/conference_track_management.rb data/test_input3.txt` has more talks so 3 tracks will be outputted instead of 2

###Results###

Test input: 
```
Writing Fast Tests Against Enterprise Rails 60min
Overdoing it in Python 45min
Lua for the Masses 30min
Ruby Errors from Mismatched Gem Versions 45min
Common Ruby Errors 45min
Rails for Python Developers lightning
Communicating Over Distance 60min
Accounting-Driven Development 45min
Woah 30min
Sit Down and Write 30min
Pair Programming vs Noise 45min
Rails Magic 60min
Ruby on Rails: Why We Should Move On 60min
Clojure Ate Scala (on my project) 45min
Programming in the Boondocks of Seattle 30min
Ruby vs. Clojure for Back-End Development 30min
Ruby on Rails Legacy App Maintenance 60min
A World Without HackerNews 30min
User Interface CSS in Rails Apps 30min
```

Program output:
```
$ ruby lib/conference_track_management.rb data/test_input.txt
Track 1:
09:00AM Writing Fast Tests Against Enterprise Rails 60min
10:00AM Clojure Ate Scala (on my project) 45min
10:45AM Overdoing it in Python 45min
11:30AM A World Without HackerNews 30min
12:00PM Lunch
01:00PM Rails Magic 60min
02:00PM Ruby on Rails: Why We Should Move On 60min
03:00PM Sit Down and Write 30min
03:30PM Woah 30min
04:00PM Ruby vs. Clojure for Back-End Development 30min
04:30PM Rails for Python Developers lightning
05:00PM Networking Event

Track 2:
09:00AM Ruby on Rails Legacy App Maintenance 60min
10:00AM Common Ruby Errors 45min
10:45AM Accounting-Driven Development 45min
11:30AM Programming in the Boondocks of Seattle 30min
12:00PM Lunch
01:00PM Communicating Over Distance 60min
02:00PM Ruby Errors from Mismatched Gem Versions 45min
02:45PM Pair Programming vs Noise 45min
03:30PM User Interface CSS in Rails Apps 30min
04:00PM Lua for the Masses 30min
05:00PM Networking Event

```

###Problem requirements and assumptions###

You are planning a big programming conference and have received many proposals which have passed the initial screen process but you're having trouble fitting them into the time constraints of the day -- there are so many possibilities! So you write a program to do it for you.

- The conference has multiple tracks each of which has a morning and afternoon session.
- Each session contains multiple talks.
- Morning sessions begin at 9am and must finish by 12 noon, for lunch.
- Afternoon sessions begin at 1pm and must finish in time for the networking event.
- The networking event can start no earlier than 4:00 and no later than 5:00.
- No talk title has numbers in it.
- All talk lengths are either in minutes (not hours) or lightning (5 minutes).
- Presenters will be very punctual; there needs to be no gap between sessions.

Note that depending on how you choose to complete this problem, your solution may give a different ordering or combination of talks into tracks. This is acceptable; you don’t need to exactly duplicate the sample output given here.

###Design and assumptions###

* System architecture of this program is similar to a Ruby gem, in order to keep things organized in a familiar way 
* To separate app components / keep it maintainable, reusable, and testable, I used the following classes and modules:
    * Track (has many sessions)
    * Session (has many talks)
    * Talk
    * Talks (contains methods on all talks)
    * Scheduling Methods

These are the basic steps I planned my methods to follow in order to output the correct schedule:

* Parse list of talks
    * Assuming only integers are talk lengths & 'lighting' talks are 5min
* Sort talks in descending order 
    * Treated this as a variation of the bin packing problem, in which it is more effective if list is sorted first
* Find out how many tracks are needed to contain all the talks 
    * From total time of talks and total time of track containing all session types (morning & night)
* For each of the talks, add longest remaining into a session with shortest total talk time so far that fits
- Format & output the track using start times

_Note: Array of session types is defined in session.rb as follows_

```
SESSION_TYPES = [ 
  { start_time: "0900", min_end_time: nil, max_end_time: "1200", end_event: "Lunch" },
  { start_time: "1300", min_end_time: "1600", max_end_time: "1700", end_event: "Networking Event" }
  #can add more sessions here, in order of appearance
]
```

If you would like to add more sessions, you can do so like this:
```
SESSION_TYPES = [ 
  { start_time: "0900", min_end_time: nil, max_end_time: "1200", end_event: "Lunch" },
  { start_time: "1300", min_end_time: "1600", max_end_time: "1700", end_event: "Networking Event" }
  { start_time: "1800", min_end_time: "2000", max_end_time: "2030", end_event: "Dinner" }
]
```
Example program output using `ruby lib/conference_track_management.rb data/test_input3.txt`
```
Track 1:
09:00AM AngularJS: Should We Move On? 60min
10:00AM Ruby on Rails: Why We Should Move On 60min
12:00PM lunchQQQQ
01:00PM Writing Fast Tests Against Enterprise Rails 60min
02:00PM Accounting-Driven Development 45min
02:45PM Ruby vs. Clojure for Back-End Development 30min
04:00PM Networking Event
06:00PM Ruby on Rails Legacy App Maintenance 60min
07:00PM Rails Magic 60min
08:00PM Dinner

Track 2:
09:00AM Communicating Over Distance 60min
10:00AM Overdoing it in Python 45min
10:45AM Lua for the Masses 30min
12:00PM Lunch
01:00PM Trends in Mobile Responsiveness 60min
02:00PM Pair Programming vs Noise 45min
02:45PM Sit Down and Write 30min
04:00PM Networking Event
06:00PM Architecting for Continuous Delivery and Zero Downtime 60min
07:00PM Woah 30min
07:30PM A World Without HackerNews 30min
08:00PM Dinner

Track 3:
09:00AM Professional Scrum Master 60min
10:00AM Common Ruby Errors 45min
10:45AM Rails for Python Developers lightning
12:00PM Lunch
01:00PM Fast Track to Scala 60min
02:00PM Ruby Errors from Mismatched Gem Versions 45min
02:45PM User Interface CSS in Rails Apps 30min
04:00PM Networking Event
06:00PM Programming in Hong Kong 60min
07:00PM Clojure Ate Scala (on my project) 45min
07:45PM Programming in the Boondocks of Seattle 30min
08:30PM Dinner

```